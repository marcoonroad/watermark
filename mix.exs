defmodule Watermark.Mixfile do
  use Mix.Project

  @moduledoc "Mix project description."

  defp description do
    "Watermark Mix Task to write metadata across documentations."
  end

  def project do
    [
      app: :watermark,
      name: "Watermark",
      version: "0.1.0",
      elixir: "~> 1.5",
      # build_embedded: Mix.env == :prod,
      # start_permanent: Mix.env == :prod,
      deps: deps(),
      description: description(),
      aliases: aliases(),
      package: package(),
      preferred_cli_env: [
              "coveralls": :test,
              "coveralls.json": :test,
              "coveralls.detail": :test,
              "coveralls.post": :test,
              "coveralls.html": :test
      ],
      test_coverage: [tool: ExCoveralls]
    ]
  end

  defp package do
      [
        name: "Watermark",
        files: [
          "config",
          "lib",
          "test",
          "mix.exs",
          "mix.lock",
          "README.md"
        ],
        maintainers: ["Marco Aurélio da Silva"],
        licenses: ["MIT"],
        links: %{"GitHub" => "https://github.com/marcoonroad/watermark"},
      ]
  end

  defp aliases do
    [
      c:  ["compile"],
      d:  ["docs"],
      dl: ["dialyzer"],
      p:  ["hex.publish", "docs", "hex.docs"],
      t:  ["test"],

      triggered: [
        "credo --strict",
        "dialyzer",
        "test",
        "coveralls"
      ]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:mock,        "~> 0.3"},
      {:faker,       "~> 0.9"},
      {:getopt,      "~> 1.0"},
      {:cf,          "~> 0.3"},
      {:dir_walker,  "~> 0.0"},
      {:fs,          "~> 3.4"},
      {:ex_doc,      "~> 0.18", only: [:dev, :prod], runtime: false},
      {:credo,       "~> 0.8",  only: [:dev, :test], runtime: false},
      {:dialyxir,    "~> 0.5",  only: [:dev, :test], runtime: false},
      {:distillery,  "~> 1.5",  only: [:dev, :prod], runtime: false},
      {:excoveralls, "~> 0.7",  only: [:dev, :test], runtime: false},
      {:proper,      "~> 1.2",  only: [:dev, :test], runtime: false}
    ]
  end
end
