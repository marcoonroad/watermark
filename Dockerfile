FROM elixir

COPY . .

RUN mix local.hex --force
RUN mix local.rebar --force
RUN mix deps.get
RUN mix compile
RUN mix triggered

ENTRYPOINT ["mix"]
