defmodule Watermark.Core.Metadata do
  @moduledoc """
  Internal module mapping Watermark labels to Mix project metadata.
  """

  alias Watermark.Core.Type

  @type pairs :: Type.pairs(atom, any)

  @doc "Wrapper around the Mix project metadata."
  @spec get(pairs, atom) :: Type.option(String.t)
  def get(metadata, label) do
    case label do
      :author      -> metadata[:package]
                   |> bind(&(&1[:maintainers]))
                   |> bind(&List.first/1)
      :version     -> metadata[:version]
      :license     -> metadata[:package]
                   |> bind(&(&1[:licenses]))
                   |> bind(&List.first/1)
      :description -> metadata[:description]
      :name        -> metadata[:package]
                   |> bind(&(&1[:name]))
                   |> otherwise(metadata[:name])
      _            -> nil
    end
  end

  @doc """
  List all available watermark metadata informations.
  """
  @spec list(pairs) :: pairs
  def list(metadata) do
    labels()
    |> Enum.map(fn label -> {label, get(metadata, label)} end)
    |> Enum.reject(fn({_, value}) -> value == nil end)
  end

  defp bind(value, rest) do
    case value do
      nil -> nil
      _   -> rest.(value)
    end
  end

  defp otherwise(first, second) do
    case first do
      nil -> second
      _   -> first
    end
  end

  defp labels, do: [:author, :version, :license, :description, :name]

  # =================================================================

  defp key({label, _}),      do: label
  defp const(value),         do: fn _ -> value end
  defp repeat(limit, value), do: 1 .. limit |> Enum.map(const value)

  defp fill(string, limit) do
    difference = (limit - String.length string) + 1

    difference |> repeat(" ") |> Enum.join
  end

  defp indent({key, value}, limit) do
    "#{fill key, limit}%%#{key}%% -> #{value}"
  end

  def show(metadata) do
    hashtable = metadata
    |> list
    |> Enum.map(fn {key, value} -> {Atom.to_string(key), value} end)

    limit = hashtable
    |> Enum.map(&key/1)
    |> Enum.map(&String.length/1)
    |> Enum.max

    hashtable
    |> Enum.map(&(indent &1, limit))
  end
end
