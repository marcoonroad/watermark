defmodule Watermark.Core.Regex do
  @moduledoc """
  Internal helper module providing useful regular
  expressions for this library.
  """

  @doc """
  Regex which detects the following filenames:

  + *.md
  + *.markdown
  + *.txt
  """
  @spec docs() :: String.t
  def docs, do: "^.+\.(md|markdown|txt)$"

  @doc """
  Regex to ignore the following directories:

  + deps/
  + _build/
  + config/
  """
  @spec ignore() :: String.t
  def ignore, do: "^.*(\\|\/)?(deps|_build|config)(\\|\/)?.*$"

  @doc """
  This regex is used to match watermark labels in
  documentation source files.
  """
  @spec label() :: String.t
  def label, do: "%%([A-Z]+)%%" # beware! match must be case insensitive
end
