defmodule Watermark.Core.Filter do
  @moduledoc """
  Internal module providing file filtering logic.
  """

  @doc """
  Filters a passed stream by positive and negative predicates.
  """
  @type patterns :: %{reject: String.t, filter: String.t}

  @spec run(list(String.t), patterns) :: list(String.t)
  def run(list, %{reject: reject_pattern, filter: filter_pattern}) do
    reject_regex = Regex.compile!(reject_pattern, "iu")
    filter_regex = Regex.compile!(filter_pattern, "iu")

    list
    |> Enum.reject(&(&1 =~ reject_regex))
    |> Enum.filter(&(&1 =~ filter_regex))
  end
end
