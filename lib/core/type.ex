defmodule Watermark.Type do
  @moduledoc """
  Helper module for public/exposed types.
  """

  @typedoc """
  Type used to represent nullable/optional values.
  """
  @type option(t) :: t | nil

  @typedoc """
  Type used to represent generic hash maps.
  """
  @type pairs(k, v) :: [{k, v}]
end
