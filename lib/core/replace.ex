defmodule Watermark.Core.Replace do
  alias Watermark.Core.Metadata
  alias Watermark.Core.Regex, as: RegexPattern

  @moduledoc """
  This internal module deals with the watermark substitution
  step.
  """

  @doc """
  Detects all the invalid labels with a plain string/text.
  """
  @spec invalid(String.t, struct) :: list(String.t)
  def invalid(text, struct) do
    RegexPattern.label
    |> Regex.compile!("iu")
    |> Regex.scan(text)
    |> Enum.map(&Enum.drop(&1, 1)) # always drops the full/entire first match
    |> Enum.concat                 # merge everything together
    |> Enum.reject(&(struct[String.to_atom &1]))
  end

  defp get(struct, label), do: Metadata.get(struct, String.to_atom label)

  @doc """
  Entry point of this module.
  """
  @spec run(String.t, struct) :: String.t
  def run(text, struct) do
    regex = Regex.compile!(RegexPattern.label, "iu")

    Regex.replace(regex, text, fn(_, label) -> get(struct, label) end)
  end
end
