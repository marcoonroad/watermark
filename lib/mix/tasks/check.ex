defmodule Mix.Tasks.Watermark.Check do
  alias Mix.Project
  alias Watermark.Core.Regex, as: RegexPattern
  alias Watermark.Core.Replace
  alias Watermark.Core.Filter
  alias Watermark.Core.Metadata
  alias Watermark.Effect.Walk

  use Mix.Task

  @shortdoc "Checks if the documents contain valid watermarks."

  @moduledoc """
  Task to check the consistency of watermark labels across your
  documentation files.

  #### Usage

  ```
  $ mix watermark.check [TARGETDIR]
  ```
  """

  defp filedata(filename) do
    %{
      file: filename,
      text: File.read! filename
    }
  end

  defp check(%{file: file, text: text}, valid) do
    invalid = Replace.invalid(text, valid)

    %{
      file:    file,
      invalid: invalid,
      status:  Enum.empty? invalid
    }
  end

  defp project(%{status: status}) do
    status
  end

  defp debug(%{file: file, invalid: list}) do
    labels = Enum.map(list, &("%%" <> &1 <> "%%"))

    """
    At file #{ file }, the following invalid labels appear:
    \t#{ Enum.join labels, ", " }
    """
  end

  defp fail?("") do
    IO.puts "Everything seems fine!"
  end

  defp fail?(reason) do
    Mix.raise ("""
    Some invalid watermark labels were detected!

    """ <> reason)
  end

  def run([]), do: run ["./"]

  def run([target]) do
    IO.puts """
    We're going to check against inexistent labels from the mix.exs mapping...
    """

    valid = Project.get!.project
      |> Metadata.list
      |> Enum.map(fn {key, value} -> {key, value != nil} end)

    filter = RegexPattern.docs
    reject = RegexPattern.ignore

    target
    |> Walk.run
    |> Filter.run(%{filter: filter, reject: reject})
    |> Enum.map(&filedata/1)
    |> Enum.map(&(check(&1, valid)))
    |> Enum.reject(&project/1)
    |> Enum.map(&debug/1)
    |> Enum.join("\n")
    |> fail?

    IO.puts ""
  end

  def run(list) do
    Mix.raise """
    Invalid number of arguments!

    Please, ensure that at most one argument is being passed.
    The passed arguments are:

    \t#{ Enum.join list, " " }
    """
  end
end
