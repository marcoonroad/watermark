defmodule Mix.Tasks.Watermark.Mark do
  alias Mix.Tasks.Watermark.Check
  alias Watermark.Core.Filter
  alias Watermark.Effect.Walk
  alias Watermark.Effect.Mark
  alias Watermark.Core.Regex, as: RegexPattern
  alias Mix.Project

  use Mix.Task

  @shortdoc "Performs the substitution of labels by respective metadata."

  @moduledoc """
  Task to write metadata on watermark labels.

  ### Usage

  ```
  $ mix watermark.mark [TARGETDIR]
  ```
  """

  def run([]), do: run ["./"]

  def run([target]) do
    Check.run [target]

    filter = RegexPattern.docs
    reject = RegexPattern.ignore

    target
    |> Walk.run
    |> Filter.run(%{filter: filter, reject: reject})
    |> Mark.run(Project.get!.project)
  end

  def run(arguments) do
    Check.run arguments
  end
end
