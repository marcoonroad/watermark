defmodule Mix.Tasks.Watermark.Show do
  alias Watermark.Core.Metadata
  alias Mix.Project

  use Mix.Task

  @shortdoc "Show all the possible watermark metadata substitutions."

  @moduledoc """
  This Mix Task shows all the available watermark metadata from mix.exs.
  Note that this set of metadata is a small subset of the real set of
  metadata which mix.exs actually provides.

  ### Usage

  ```
  $ mix watermark.show
  ```
  """

  def run([]) do
    IO.puts """
    The following substitutions are possible:
    """

    Project.get!.project
    |> Metadata.show
    |> Enum.each(&IO.puts/1)

    IO.puts ""
  end

  def run(_), do:
    Mix.raise """
    This mix task expects no one argument!
    """
end
