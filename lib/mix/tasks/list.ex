defmodule Mix.Tasks.Watermark.List do
  alias Watermark.Core.Regex, as: RegexPattern

  use Mix.Task

  @shortdoc "List documentation files to watermark."

  @moduledoc """
  This task show all the available documentation files
  in the project directory. Optionally, you could refine
  such search by passing an explicit target directory.

  ## Examples

  ```
    $ mix watermark.list # list all doc files from here (./)
  ```

  ```
    $ mix watermark.list lib # list all docs within lib/
  ```
  """

  def run([]), do: run ["./"]

  def run([directory]) do
    filter = RegexPattern.docs
    reject = RegexPattern.ignore

    directory
    |> Watermark.walk
    |> Watermark.filter(%{filter: filter, reject: reject})
    |> Enum.each(&IO.puts/1)
  end

  def run(list) do
    arguments = list |> Enum.join(" ")

    Mix.raise ("""
    Invalid number of arguments!

    Please, ensures that at most one argument is being passed.
    The passed arguments are:

    """ <> "\t" <> arguments)
  end
end
