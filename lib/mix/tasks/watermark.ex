defmodule Mix.Tasks.Watermark do
  use Mix.Task

  @shortdoc "Information about all the available tasks."

  @moduledoc """
  Prints the list of available Mix Tasks within the Watermark package.

  ### Usage

  Simply call:

  ```
  $ mix watermark
  ```
  """

  def run(_) do
    IO.puts """
    Available task commands are:

      $ mix watermark.check [DIRECTORY]
      $ mix watermark.get   [LABEL1 [LABEL2 [LABEL3 ...]]]
      $ mix watermark.list  [DIRECTORY]
      $ mix watermark.mark  [DIRECTORY]
      $ mix watermark.show
    """
  end
end
