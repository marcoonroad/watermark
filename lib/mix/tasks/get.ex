defmodule Mix.Tasks.Watermark.Get do
  alias Watermark.Core.Metadata
  alias Mix.Project

  use Mix.Task

  @shortdoc "Show the current watermark substitution for given labels."

  @moduledoc """
  Task to inspect some given watermark labels.

  ### Usage

  ```
  $ mix watermark.get LABEL1 LABEL2 LABEL3 etc...
  ```

  The associated output follows in the same order, for example:

  ```
  VALUE1
  VALUE2
  VALUE3
  ...
  ```
  """

  def run([]) do
    Mix.raise "This task expects at least one watermark label!"
  end

  def run(list) do
    project = Project.get!.project
    labels  = Enum.map(list, &({&1, Metadata.get(project, String.to_atom &1)}))
    invalid = Enum.filter(labels, fn {_, value} -> value == nil end)

    if Enum.empty? invalid do
      labels
      |> Enum.map(fn {_, value} -> value end)
      |> Enum.each(&IO.puts/1)

    else
      Mix.raise """
      Invalid watermark labels passed as argument!

      The following watermark labels don't exist in this project:
      \t#{
        invalid
        |> Enum.map(fn {key, _} -> "%%" <> key <> "%%" end)
        |> Enum.join(", ")
      }
      """
    end
  end
end
