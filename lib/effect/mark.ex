defmodule Watermark.Effect.Mark do
  alias Watermark.Core.Replace
  @moduledoc """
  Effectful module which updates the watermark
  labels across the detected documentation files.
  """

  defp update(filename, metadata) do
    text = File.read! filename
    marked = Replace.run text, metadata

    cond do
      marked == "" ->
        raise "Error: replacement engine contains a really bad bug!"

      marked == text ->
        IO.puts "Nothing to change on file #{ filename }..."

      true ->
        file = File.open!(filename, [:write, :utf8])
        previousname = filename <> ".previous"

        "Watermarking the documentation file #{ filename }..."
        |> IO.puts

        "Previous state before watermarking is available as #{ previousname }..."
        |> IO.puts

        try do
          previous = File.open!(previousname, [:write, :utf8])

          IO.write previous, text
          File.close previous
          IO.write file, marked

          IO.puts "Congratulations: Watermarking was performed successfully!"

        rescue _ ->
          "Warning: failed to write previous state for file #{ previousname }!"
          |> IO.puts

          "Warning: Watermarking have been ignored for file #{ filename }!"
          |> IO.puts
        end

        File.close file
    end

    IO.puts ""
  end

  @doc """
  Watermark generator for passed documentations.

  Receives a list of filenames.
  Returns nothing.
  """
  @spec run(list, struct) :: :ok
  def run(list, metadata), do: list |> Enum.each(&update(&1, metadata))
end
