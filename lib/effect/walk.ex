defmodule Watermark.Effect.Walk do
  require DirWalker

  @moduledoc """
  Effectful module which provides recursive search
  of files matching some kind of documentation.
  """

  @doc """
  Entry point of this module.

  Receives a target directory as a string.
  Returns a list of all recursive files within a directory.
  """
  @spec run(String.t) :: list(String.t)
  def run(directory \\ "./") do
    directory
    |> DirWalker.stream
    |> Enum.to_list
  end
end
