defmodule Watermark do
  @moduledoc """
  Entry-point module for public watermark functions.
  """

  alias Watermark.Core.Filter
  alias Watermark.Effect.{Walk, Mark}

  @doc """
  Delegate function to Watermark.Core.Filter.run/2
  """
  defdelegate filter(list, patterns), to: Filter, as: :run

  @doc """
  Delegate function to Watermark.Effect.Walk.run/1
  """
  @spec walk(String.t) :: list(String.t)
  defdelegate walk(directory), to: Walk, as: :run

  @doc """
  Delegate function to Watermark.Effect.Mark.run/2
  """
  @spec mark(list, struct) :: :ok
  defdelegate mark(filenames, metadata), to: Mark, as: :run
end
